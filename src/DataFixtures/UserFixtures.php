<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setApiToken('oui');
        $user->setEmail('oui@oui.fr');
        $user->setPassword('oui');
        $manager->persist($user);
        $manager->flush();
    }
}
