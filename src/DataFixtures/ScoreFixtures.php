<?php

namespace App\DataFixtures;

use App\Entity\Score;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ScoreFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i = 1; $i < 10; $i++){
            $score = new Score();
            $score->setScore($i);
            $score->setName('name '.$i);
            $score->setDate(new \DateTime('06/04/2014'));
            $manager->persist($score);
        }

        $manager->flush();
    }
}
