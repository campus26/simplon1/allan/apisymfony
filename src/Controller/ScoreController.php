<?php

namespace App\Controller;

use App\Entity\Score;
use App\Repository\ScoreRepository;
use Doctrine\DBAL\Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ScoreController extends AbstractController
{
    /**
     * @Route("/score", name="score")
     */
    public function index(ScoreRepository $scoreRepository, Request $request): Response
    {
        if($request->headers->get('X-AUTH-TOKEN') !== null) {
            $oui = $scoreRepository->findBestScore();
            return $this->json($oui);
        } else{
            return new Response('bad token',401);
        }
    }

    /**
     * @Route("/newscore", name="newscore")
     */
    public function add(Request $request): Response
    {
        if($request->headers->get('X-AUTH-TOKEN') !== null){
            $data = $request->getContent();
            try {
                $object = $this->get('serializer')->deserialize($data, Score::class, 'json');
            }catch (Exception $e){
                echo 'Exception reçue : ',  $e->getMessage(), "\n";
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($object);
            $entityManager->flush();

            return new Response(
                '<html><body>ajout de données</body></html>'
            );
        } else {
            return new Response('bad token',401);
        }
    }

}
